var generateJimboHTML = function() {
  var div = document.createElement("div");
  div.className = "pay-me-pay-me-pay-me";
  div.innerHTML =
    "<div class='my-face'><img src='/.static/robi-fade.png' /></div>" +
    "<div class='please-read-important-message-from-wikimedia-foundation'>" +
    "<h2>From JulayWorld founder Robi Pires</h2><hr />" +
    "<p>JulayWorld is one of the most visited websites in the world.</p>" +
    "<p>Commerce is fine. Advertising is not evil. But it doesn't belong here. Not in JulayWorld.</p>" +
    "<p>JulayWorld is something special. It is like a library or a public park. It is like a temple for the mind. It is a place we can all go to think, to learn, to share our knowledge with others.</p>" +
    "<p>When I founded JulayWorld, I could have made it into a for-profit company with advertising banners, but I decided to do something different. We’ve worked hard over the years to keep it lean and tight. We fulfill our mission efficiently.</p>" +
    "<p>If everyone reading this donated, our fundraiser would be done within an hour and we'd have enough money to pay Ethan Ralph's student loans. But not everyone can or will donate. And that's fine. Each year just enough people decide to give.</p>" +
    "<p>This month, please consider making a donation of $1, $2, $5 or whatever you can to protect and sustain JulayWorld.</p>" +
    "<p>Thanks,</p>" +
    "<p><strong>Robi Pires</strong><br />" +
    "JulayWorld Founder</p>" +
    "</div>" +
    "<div class='pay-me-here'>" +
    "<div class='paypig-box'><h3>Donation options</h3>" +
    "<p class='paypig-option'><strong>BTC:</strong> 1LK5C1oHBMiGwc3nSGa8Xioc6AApy5NBk3</p>" +
    "<p class='paypig-option'><strong>ETH:</strong> 0x5B9ab40e2098F5eF0c88fEe2e594BA318752a788</p>" +
    "<div class='where-your-donation-goes' data-answer='mongolia-aint-cheap-god-dammit'>" +
    "<h3>Where your donation goes</h3>" +
    "<p><strong>Technology:</strong> Servers, bandwidth, maintenance, development. JulayWorld is one of the top 10 websites in the world, and it runs on a fraction of what other top websites spend.</p>" +
    "<p><strong>People and Projects:</strong> The other top websites have thousands of employees. We have about 350 staff and contractors to support a wide variety of projects, making your donation a great investment in a highly-efficient not-for-profit organization.</p>" +
    "</div></div>" +
    "<a class='fuck-off-forever'>Do not show this to me again</a>";

  div.querySelector(".fuck-off-forever").addEventListener("click", () => {
    localStorage.setItem("jimbo-read", "true");
    document.body.removeChild(div);
  });

  return div;
};

var miniCSS =
  ".pay-me-pay-me-pay-me{position:fixed;z-index:100;top:20px;left:0;width:100%;height:70%;background-color:#f6f6f6;background-image:linear-gradient(to bottom, #ffffff 40px, #f6f6f6 80px);color:black;font-family:sans-serif;font-size:13px;padding-top:20px;box-sizing:border-box;display:flex;justify-content:space-between;overflow-y:auto}.my-face{position:fixed;height:70%;z-index:-1}.my-face img{height:100%;margin-top:-20px}.please-read-important-message-from-wikimedia-foundation{margin-left:70px;margin-right:20px}.please-read-important-message-from-wikimedia-foundation p{margin-bottom:10px}.paypig-box{margin:30px;padding:10px;background-color:white;border:1px solid #BBB;border-radius:2px}.paypig-box h3{text-align:center;margin-bottom:10px}.paypig-option{border:1px solid #888;background-color:#E3E3E3;padding:10px 20px}.paypig-option + div{margin-top:20px}.fuck-off-forever{position:absolute;top:20px;right:20px;color:darkblue;text-shadow:none !important}";

document.addEventListener(
  "DOMContentLoaded",
  () => {
    if (!localStorage.getItem("jimbo-read")) {
      document.body.appendChild(generateJimboHTML());
      var style = document.createElement("style");
      style.innerHTML = miniCSS;
      document.head.appendChild(style);
    }
  },
  false
);
